const fs = require('fs');

const wordFilePath = 'five-letter-english-words.txt';


function wordleAssist(possibleWords) {
    // Print "header" with initial possible word count
    const startingWordCount = possibleWords.length;
    console.log(`Starting with ${startingWordCount.toLocaleString()} possible 5-letter words...`);

    // TODO:
    // Apply filtering to possibleWords based on Wordle hints here...

    // Print comma-separated list of remaining possibleWords
    console.log(possibleWords.join(", "))

    // Print stats summary for possibleWords
    const endingWordCount = possibleWords.length;
    const decrease = startingWordCount - endingWordCount;
    let decreasePercent = 0;
    if (decrease) {
        decreasePercent = Math.round((decrease / startingWordCount) * 100, 2)
    }
    console.log(`Ending with ${endingWordCount.toLocaleString()} remaining 5-letter words, a decrease of ${decreasePercent}%`)
}


function loadPossibleWords(filePath) {
    var contents = fs.readFileSync(filePath, 'utf8');

    // Split into lines on line break
    return contents.split("\n")
        // Filter out lines that start with #
        .filter( (value) => !value.startsWith("#"))
        // Trim whitespace from lines
        .map( (value) => value.trim() )
        // Omit empty lines
        .filter( (value) => value);
}


wordleAssist(loadPossibleWords(wordFilePath));

